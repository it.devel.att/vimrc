call plug#begin('~/.vim/plugged')
Plug 'rust-lang/rust.vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'rust-lang-nursery/rustfmt'
Plug 'preservim/nerdtree'
Plug 'dense-analysis/ale'
Plug 'tomlion/vim-solidity'
Plug 'airblade/vim-gitgutter'
Plug 'Yggdroot/indentLine'
call plug#end()

set number
let g:rustfmt_autosave = 1
let g:ale_linters = {'rust': ['analyzer']}

set listchars=tab:›\ ,eol:¬,trail:⋅
set list
let g:indentLine_char = '¦'
let g:indentLine_leadingSpaceEnabled = 1
let g:indentLine_leadingSpaceChar = '⋅'

" Git faster updates
set updatetime=250

syntax on

autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType js setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType ts setlocal ts=2 sts=2 sw=2 expandtab
autocmd FileType go setlocal ts=4 sts=4 sw=4 expandtab

if has("termguicolors")
    set termguicolors
endif

set background=dark
colorscheme onedark

autocmd VimEnter * NERDTree | wincmd p
" On new tab open NERDTree also
autocmd BufWinEnter * NERDTreeMirror
" Autosave in text changed
" autocmd TextChanged,TextChangedI <buffer> silent write

" Close NERDTree if last file closed
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
